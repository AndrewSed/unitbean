package ru.unitbean.tarticle.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.FileStore;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Document(collection = "articles")
public class Article {
    @Id
    private String id;

    private String title;
    private String filename;
//    @Transient
//    private MultipartFile file;
//    private File image;
    private String description;
    private Date date;

    public Article() {}
    public Article(String title, Date date, String description) {
        this.title = title;
        this.date = date;
        this.description = description;
    }
    public Article(String filename, String title, Date date, String description) {
        this.filename = filename;
        this.title = title;
        this.date = date;
        this.description = description;
    }

//    public MultipartFile getFile() {
//        return file;
//    }
//
//    public void setFile(MultipartFile file) {
//        this.file = file;
//    }

    public String getId() {
        return id;
    }

    public String getFilename() {
        return filename;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM, yyyy");
        return dateFormat.format(date);
    }

    public String getDescription() {
        return description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
