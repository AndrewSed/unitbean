package ru.unitbean.tarticle.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.unitbean.tarticle.MyDateFormatSymbols;

import javax.persistence.JoinColumn;
import java.text.SimpleDateFormat;
import java.util.Date;

@Document(collection = "comments")
public class Comment {
    @Id
    private String id;

    private String articleId;
    private Date date;
    private String text;

//    @JoinColumn(name = "user_id")
//    private User author;
    private String userId;
    private String username;
    private String userImg;

    public Comment() {}
    public Comment(String userId, String username, String userImg, Date date, String text, String articleId) {
        this.userId = userId;
        this.username = username;
        this.userImg = userImg;

        this.date = date;
        this.text = text;
        this.articleId = articleId;
    }

    public String getUserImg() {
        return userImg;
    }
    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String article_id) {
        this.articleId = article_id;
    }

    public String getId() {
        return id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM yyyy", new MyDateFormatSymbols());
        return dateFormat.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
