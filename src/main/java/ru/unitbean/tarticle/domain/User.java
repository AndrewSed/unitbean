package ru.unitbean.tarticle.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;


@Document(collection = "users")
public class User implements UserDetails {
    @Id
    private String id;
    @Indexed(unique = true)
    private String username;
    private String password;
    private String imgname;
    private boolean active;
//    @BsonId
//    private String imageId;

    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

    public User(String username, String password, Set<Role> roles, String imgname) {
        this.username = username;
        this.password = password;
        this.imgname = imgname;
        this.roles = roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActive();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getImgname() {
        return imgname;
    }

    public void setImgname(String imgname) {
        this.imgname = imgname;
    }

    /*
//    public String getImageId() {
//        return imageId;
//    }

//    @Autowired
//    private UsersRepository repository;
//    @Autowired
//    private MongoTemplate mongoTemplate;
//    @Autowired
//    private GridFsTemplate gridFsTemplate;

//    public String getImage() {
//        Query query = Query.query(GridFsCriteria.where("imageId").is(imageId));
//        CommandResult image = mongoTemplate.find(query, );
//        Update update = new Update().set("imageId", Objects.requireNonNull(gridFsTemplate.findOne(query1)).getObjectId());
//        UpdateResult result = mongoTemplate.upsert(query, update, User.class);
//        return image;
//    }

//    public void setImageId(String imageId) {
//        this.imageId = imageId;
//    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
//                ", imageId='" + imageId + '\'' +
                '}';
    }
*/

    /*private enum Role {
        USER("USER"),
        GUEST("GUEST"),
        ADMIN("ADMIN");

        private final String role;
        Role(String role) {
            this.role = role;
        }
        public String getRole(){
            return role;
        }
    }*/
}
