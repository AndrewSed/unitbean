package ru.unitbean.tarticle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.unitbean.tarticle.domain.Article;
import ru.unitbean.tarticle.repo.ArticlesRepository;

import java.util.List;

@Service
public class ArticleService {
    @Autowired
    private ArticlesRepository repository;

    public List<Article> loadAllArticle() {
        return repository.findAll();
    }

    public Article getArticleById(String id) {
        return repository.findArticleById(id);
    }

    public Article getArticleByTitle(String title) {
        return repository.findArticleByTitle(title);
    }

    public Article save(Article article) { return repository.save(article); }
}
