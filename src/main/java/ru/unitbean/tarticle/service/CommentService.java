package ru.unitbean.tarticle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.unitbean.tarticle.domain.Comment;
import ru.unitbean.tarticle.repo.CommentsRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {
    @Autowired
    private CommentsRepository repository;

    public Optional<Comment> getCommentById(String id) {
        return repository.findById(id);
    }
    public List<Comment> loadByArticleId(String articleId) {
        return repository.findAllByArticleIdOrderByDateAsc(articleId);
    }

    public List<Comment> loadCommentsDesc(String articleId) {
        return repository.findAllByArticleIdOrderByDateDesc(articleId);
    }
    public List<Comment> loadCommentsAsc(String articleId) {
        return repository.findAllByArticleIdOrderByDateAsc(articleId);
    }
    public void save(Comment comment) {
        repository.save(comment);
    }
}
