package ru.unitbean.tarticle.service;

import ru.unitbean.tarticle.domain.User;
import ru.unitbean.tarticle.repo.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UsersRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByUsername(username);
    }
    public User findByUsername(String username) throws UsernameNotFoundException {
        return repository.findByUsername(username);
    }
    public List<User> loadAllUsers() {
        return repository.findAll();
    }
}