package ru.unitbean.tarticle.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import ru.unitbean.tarticle.service.UserService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService usersService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/", "/articles", "/static/*", "/css/*", "/img/**").permitAll()
                    .anyRequest().authenticated()
                .and()
                    .formLogin()
//                  .loginPage("/login")
                    .permitAll()
                .and()
                    .logout().permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(usersService)
                .passwordEncoder(NoOpPasswordEncoder.getInstance());
    }

//    @Bean
//    public UserDetailsService userDetailsService() {
//        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
//
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        for (ru.unitbean.tarticle.domain.User user : usersService.loadAllUsers())
//        {
//            UserDetails user_template = User.withUsername(user.getUsername()).
//                    password(encoder.encode(user.getPassword())).
//                    roles("USER").
//                    build();
//            manager.createUser(user_template);
//        }

/*//        UserDetails user = User.withUsername("user").
//                password(encoder.encode("password")).
//                roles("USER").build();
//        UserDetails guest = User.withUsername("guest").
//                password("").
//                roles("GUEST"). build();
//        UserDetails admin = User.withUsername("admin").
//                password(encoder.encode("admin")).
//                roles("GUEST","USER", "ADMIN"). build();

//        manager.createUser(user);
//        manager.createUser(guest);
//        manager.createUser(admin);*/
//        return manager;
//    }
}
