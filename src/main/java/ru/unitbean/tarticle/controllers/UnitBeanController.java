package ru.unitbean.tarticle.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.unitbean.tarticle.domain.Article;
import ru.unitbean.tarticle.repo.ArticlesRepository;
import ru.unitbean.tarticle.service.ArticleService;

import java.io.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Controller
public class UnitBeanController {
    @Autowired
    private ArticleService articles;

    @GetMapping({"/", "/articles"})
    public String getArticles(Model model) throws FileNotFoundException {
        model.addAttribute("articles", articles.loadAllArticle());
//        String filename = "avatar_man_people_person_profile_user_icon_123380";
//        File file = new File("D:\\Java\\Projects\\taricle\\src\\main\\resources\\static\\images\\avatars\\5da71979e326bd39caaa3d80.png");
//        FileInputStream stream = new FileInputStream(file);
//        gridFsTemplate.store(stream, filename);
//
//        Query query = new Query(where("username").is("Den4ik7755"));
//        Query query1 = Query.query(GridFsCriteria.whereFilename()
//                .is("avatar_man_people_person_profile_user_icon_123380"));
//        Update update = new Update().set("imageId", Objects.requireNonNull(gridFsTemplate.findOne(query1)).getObjectId());
//        UpdateResult result = mongoTemplate.upsert(query, update, User.class);
        return "main";
    }


}
