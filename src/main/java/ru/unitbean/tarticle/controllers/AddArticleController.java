package ru.unitbean.tarticle.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.unitbean.tarticle.domain.Article;
import ru.unitbean.tarticle.service.ArticleService;

import javax.servlet.annotation.MultipartConfig;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Controller
@MultipartConfig
public class AddArticleController {

    @Autowired
    private ArticleService articlesService;
    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("/add_article")
    public String formAddArticle() {
        return "add_article";
    }

    @PostMapping("/add_article")
    public String postArticle(
            @RequestParam("title") String title,
            @RequestParam("description") String description, Model model,
            @RequestParam("file") MultipartFile file
    ) throws IOException {
        Article article = new Article(title, new Date(), description);

        if (file != null && !Objects.requireNonNull(file.getOriginalFilename()).isEmpty()) {

            File uploadDir = new File(System.getProperty("user.dir").replace('\\', '/')
                    + uploadPath);

            if (!uploadDir.exists()) {
                if(uploadDir.mkdir()) {
                    System.out.println("Create uploads dir: "
                            + System.getProperty("user.dir").replace('\\', '/')
                            + uploadPath);
                }
                else {
                    model.addAttribute("msg", "Error path");
                    model.addAttribute("description", "Error with create uploads dir");
                    return "error_adding";
                }
            }

            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();

            file.transferTo(new File(System.getProperty("user.dir").replace('\\', '/')
                    + uploadPath + "/" + resultFilename));
            article.setFilename(resultFilename);
            articlesService.save(article);
            return "successful_adding";
        }
        model.addAttribute("msg", "Error with upload file. ");
        model.addAttribute("description", "File is null");
        return "error_adding";
    }

    @GetMapping("/successful_adding")
    public String mesSuccessAdd() {
        return "successful_adding";
    }

    @GetMapping("/error_adding")
    public String mesErrAdd() {
        return "error_adding";
    }

}
