package ru.unitbean.tarticle.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.unitbean.tarticle.domain.Article;
import ru.unitbean.tarticle.domain.Comment;
import ru.unitbean.tarticle.domain.User;
import ru.unitbean.tarticle.service.ArticleService;
import ru.unitbean.tarticle.service.CommentService;
import ru.unitbean.tarticle.service.UserService;

import java.util.Date;

@Controller
@RequestMapping({"/articles/article"})
public class ArticleController {
    @Autowired
    private ArticleService articles;
    @Autowired
    private CommentService comments;
    @Autowired
    private UserService users;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getArticle(@AuthenticationPrincipal User usr,
                             @PathVariable(name="id") String id,
                             Model model) {

        Article article = articles.getArticleById(id);
        if (article == null) {
            model.addAttribute("id", id);
            return "notfounderror";
        }
        ru.unitbean.tarticle.domain.User ur = users.findByUsername(usr.getUsername());
        model.addAttribute("article", article);
        model.addAttribute("user", ur);

        model.addAttribute("comments", comments.loadCommentsDesc(id));
        return "article";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public String postComment(@AuthenticationPrincipal User usr,
                              @PathVariable String id,
                              @ModelAttribute Comment comment,
                              Model model) {
        Article article = articles.getArticleById(id);
        if (article == null) {
            model.addAttribute("id", id);
            return "notfounderror";
        }
//        User userDetails = (User) org.springframework.security.core.context.SecurityContextHolder
//                .getContext().getAuthentication().getPrincipal();
        User author = users.findByUsername(usr.getUsername());

        Date currentDate = new Date();

        comment.setArticleId(id);
        comment.setDate(currentDate);
        comment.setUserId(author.getId());
        comment.setUsername(author.getUsername());
        comment.setUserImg(author.getImgname());
        comments.save(comment);

        model.addAttribute("article", article);
        model.addAttribute("user", author);
        model.addAttribute("comments", comments.loadCommentsDesc(id));
        return this.getArticle(author, id, model);
    }
}
