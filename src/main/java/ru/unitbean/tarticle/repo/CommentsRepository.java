package ru.unitbean.tarticle.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import ru.unitbean.tarticle.domain.Comment;

import java.util.List;
import java.util.Optional;

//@Component
public interface CommentsRepository extends MongoRepository<Comment, String> {
    public Optional<Comment> findById(String id);
    public List<Comment> findByArticleId(String article_id);
    public List<Comment> findAllByArticleIdOrderByDateAsc(String articleId);
    public List<Comment> findAllByArticleIdOrderByDateDesc(String articleId);
}
