package ru.unitbean.tarticle.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.unitbean.tarticle.domain.Article;
import org.springframework.stereotype.Component;

//@Component
public interface ArticlesRepository extends MongoRepository<Article, String> {
    public Article findArticleById(String id);
    public Article findArticleByTitle(String title);
}
