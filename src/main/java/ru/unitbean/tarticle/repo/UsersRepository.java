package ru.unitbean.tarticle.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.unitbean.tarticle.domain.User;

public interface UsersRepository extends MongoRepository<User, String> {
    public User findUserById(String id);
    public User findByUsername(String username);
}
